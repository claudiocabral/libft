/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 14:53:33 by ccabral           #+#    #+#             */
/*   Updated: 2017/08/08 20:56:11 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	ft_is_in_base(const char *str, const char *base)
{
	const char	*head;
	const char	*str_head;

	head = base;
	str_head = str;
	while (*str)
	{
		while (*base)
		{
			if (*str == *base)
				break ;
			++base;
			if (!(*base) && str == str_head)
				return (0);
		}
		base = head;
		++str;
	}
	return (1);
}

static int	ft_is_base_valid2(const char *str, const char *base)
{
	const char	*head;
	const char	*iter;

	head = base;
	iter = base;
	while (*iter)
	{
		if (!(*iter) || *iter == '+' || *iter == '-'
				|| *iter < ' ' || *iter == 127)
			return (0);
		base = iter + 1;
		while (*base)
		{
			if (*base == *iter)
				return (0);
			++base;
		}
		++iter;
	}
	if (!ft_is_in_base(str, head))
		return (0);
	return (1);
}

static long	ft_get_base_size2(const char *base)
{
	int	size;

	size = 0;
	while (*base)
	{
		++base;
		++size;
	}
	return (size);
}

static int	ft_get_num(const char c, const char *base)
{
	int	i;

	i = 0;
	while (c != *base && *base)
	{
		++i;
		++base;
	}
	return (i);
}

int			ft_atoi_base(const char *str, const char *base)
{
	int		signal;
	long	num;
	int		size;

	num = 0;
	signal = 1;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			signal *= -1;
		++str;
	}
	if (!ft_is_base_valid2(str, base))
		return (0);
	size = ft_get_base_size2(base);
	while (ft_get_num(*str, base) == size)
		++str;
	while (*str && ft_get_num(*str, base) != size)
	{
		num *= size;
		num += ft_get_num(*str, base);
		++str;
	}
	num *= signal;
	return (num);
}
