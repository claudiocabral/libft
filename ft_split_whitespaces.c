/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/07 18:27:11 by ccabral           #+#    #+#             */
/*   Updated: 2017/11/06 17:42:32 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	ft_is_separator(const char c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	return (0);
}

static int	ft_word_length(const char *str)
{
	int		size;

	while (*str)
	{
		if (!ft_is_separator(*str))
			break ;
		++str;
	}
	size = 0;
	while (*str)
	{
		if (ft_is_separator(*str))
			break ;
		++size;
		++str;
	}
	return (size - 1);
}

static int	ft_count_words(const char *str)
{
	int	size;

	size = 0;
	while (*str)
	{
		while (!ft_is_separator(*str) && *str)
			++str;
		++size;
		while (ft_is_separator(*str) && *str)
			++str;
	}
	return (size);
}

char		**ft_split_whitespaces(const char *str)
{
	char	**word;
	int		nbr_words;
	int		size;
	int		i;

	size = 0;
	i = 0;
	while (ft_is_separator(*str))
		++str;
	nbr_words = ft_count_words(str);
	word = (char **)malloc(sizeof(char *) * (nbr_words + 1));
	while (i < nbr_words)
	{
		size = ft_word_length(str) + 1;
		word[i] = (char *)malloc(sizeof(char) * size);
		ft_strncpy(word[i], str, size);
		str += size;
		while (ft_is_separator(*str))
			++str;
		++i;
	}
	word[i] = 0;
	return (word);
}
