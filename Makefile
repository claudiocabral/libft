# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ccabral <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/24 13:19:14 by ccabral           #+#    #+#              #
#    Updated: 2017/11/16 14:13:10 by ccabral          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	libft.a
CC			=	cc 
CFLAGS		=	-Wall -Wextra -Werror
INCLUDES	=	-I.
SRC_DIR		=	$(dir $(realpath $(firstword $(MAKEFILE_LIST))))
OBJ_DIR		=	$(dir $(realpath $(firstword $(MAKEFILE_LIST))))/objs
OBJS		=	$(OBJ_DIR)/ft_any.o \
			$(OBJ_DIR)/ft_lstnew.o \
			$(OBJ_DIR)/ft_lstpush_back.o \
			$(OBJ_DIR)/ft_lstdelone.o \
			$(OBJ_DIR)/ft_lstdel.o \
			$(OBJ_DIR)/ft_lstadd.o \
			$(OBJ_DIR)/ft_lstiter.o \
			$(OBJ_DIR)/ft_lstmap.o \
			$(OBJ_DIR)/ft_atoi_base.o \
			$(OBJ_DIR)/ft_atoi.o \
			$(OBJ_DIR)/ft_bzero.o \
			$(OBJ_DIR)/ft_count_if.o \
			$(OBJ_DIR)/ft_puterror.o \
			$(OBJ_DIR)/ft_foreach.o \
			$(OBJ_DIR)/ft_isalnum.o \
			$(OBJ_DIR)/ft_isalpha.o \
			$(OBJ_DIR)/ft_isascii.o \
			$(OBJ_DIR)/ft_isdigit.o \
			$(OBJ_DIR)/ft_isprint.o \
			$(OBJ_DIR)/ft_is_sort.o \
			$(OBJ_DIR)/ft_itoa.o \
			$(OBJ_DIR)/ft_map_int.o \
			$(OBJ_DIR)/ft_max.o \
			$(OBJ_DIR)/ft_memalloc.o \
			$(OBJ_DIR)/ft_memccpy.o \
			$(OBJ_DIR)/ft_memchr.o \
			$(OBJ_DIR)/ft_memcmp.o \
			$(OBJ_DIR)/ft_memcpy.o \
			$(OBJ_DIR)/ft_memdel.o \
			$(OBJ_DIR)/ft_memmove.o \
			$(OBJ_DIR)/ft_memset.o \
			$(OBJ_DIR)/ft_putchar.o \
			$(OBJ_DIR)/ft_putchar_fd.o \
			$(OBJ_DIR)/ft_putendl.o \
			$(OBJ_DIR)/ft_putendl_fd.o \
			$(OBJ_DIR)/ft_putnbr_base.o \
			$(OBJ_DIR)/ft_putnbr.o \
			$(OBJ_DIR)/ft_putnbr_fd.o \
			$(OBJ_DIR)/ft_putstr.o \
			$(OBJ_DIR)/ft_putstr_fd.o \
			$(OBJ_DIR)/ft_putstr_non_printable.o \
			$(OBJ_DIR)/ft_realloc.o \
			$(OBJ_DIR)/ft_split_whitespaces.o \
			$(OBJ_DIR)/ft_strcapitalize.o \
			$(OBJ_DIR)/ft_strcat.o \
			$(OBJ_DIR)/ft_strchr.o \
			$(OBJ_DIR)/ft_strclr.o \
			$(OBJ_DIR)/ft_strcmp.o \
			$(OBJ_DIR)/ft_strcpy.o \
			$(OBJ_DIR)/ft_strdel.o \
			$(OBJ_DIR)/ft_strdup.o \
			$(OBJ_DIR)/ft_strequ.o \
			$(OBJ_DIR)/ft_str_is_alpha.o \
			$(OBJ_DIR)/ft_str_is_lowercase.o \
			$(OBJ_DIR)/ft_str_is_numeric.o \
			$(OBJ_DIR)/ft_str_is_printable.o \
			$(OBJ_DIR)/ft_str_is_uppercase.o \
			$(OBJ_DIR)/ft_striter.o \
			$(OBJ_DIR)/ft_striteri.o \
			$(OBJ_DIR)/ft_strjoin.o \
			$(OBJ_DIR)/ft_strlcat.o \
			$(OBJ_DIR)/ft_strlcpy.o \
			$(OBJ_DIR)/ft_strlen.o \
			$(OBJ_DIR)/ft_strlowcase.o \
			$(OBJ_DIR)/ft_strmap.o \
			$(OBJ_DIR)/ft_strmapi.o \
			$(OBJ_DIR)/ft_strncat.o \
			$(OBJ_DIR)/ft_strncmp.o \
			$(OBJ_DIR)/ft_strncpy.o \
			$(OBJ_DIR)/ft_strnequ.o \
			$(OBJ_DIR)/ft_strnew.o \
			$(OBJ_DIR)/ft_strnstr.o \
			$(OBJ_DIR)/ft_strrchr.o \
			$(OBJ_DIR)/ft_strsplit.o \
			$(OBJ_DIR)/ft_strstr.o \
			$(OBJ_DIR)/ft_strsub.o \
			$(OBJ_DIR)/ft_strtrim.o \
			$(OBJ_DIR)/ft_strupcase.o \
			$(OBJ_DIR)/ft_swap.o \
			$(OBJ_DIR)/ft_tolower.o \
			$(OBJ_DIR)/ft_toupper.o

CDEBUG		=	-g

ifeq ($(DEBUG), 1)
	CFLAGS += $(CDEBUG)
endif

.PHONY: all clean fclean re

.NOTPARALLEL: $(NAME)

all: $(NAME)

$(NAME): $(OBJS)
	@ar rcs $(NAME) $(OBJS)


$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@[[ -d objs ]] || mkdir -p objs
	@$(CC) -c $< -o $@  $(CFLAGS) $(INCLUDES)

clean:
	@rm -rf objs

fclean: clean
	@rm -rf $(NAME)

re: fclean all
